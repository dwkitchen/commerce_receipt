<?php
/**
 * @file
 * Callbacks for receipt add pages.
 */

use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Prepares variables for list of available receipt type templates.
 *
 * Default template: commerce-receipt-add-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of receipt types.
 */
function template_preprocess_commerce_receipt_add_list(&$variables) {
  if (!empty($variables['types'])) {
    foreach ($variables['types'] as $receipt_type) {
      $url = Url::fromRoute('entity.commerce_receipt.add_form', array('commerce_receipt_type' => $receipt_type->id()));

      $variables['types'][$receipt_type->id()] = array(
        'type' => $receipt_type->id(),
        'add_link' => \Drupal::l($receipt_type->label(), $url),
        'description' => Xss::filterAdmin($receipt_type->getDescription()),
      );
    }
  }
}

