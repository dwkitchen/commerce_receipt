<?php

/**
 * @file
 * Contains Drupal\commerce_receipt\Form\CommerceReceiptTypeForm.
 */

namespace Drupal\commerce_receipt\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommerceReceiptTypeForm extends EntityForm {

  /**
   * The receipt type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $receiptTypeStorage;

  /**
   * Create an IndexForm object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $receipt_type_storage
   *   The receipt type storage.
   */
  public function __construct(EntityStorageInterface $receipt_type_storage) {
    // Setup object members.
    $this->receiptTypeStorage = $receipt_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityManagerInterface $entity_manager */
   $entity_manager = $container->get('entity.manager');
   return new static($entity_manager->getStorage('commerce_receipt_type'));
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $receipt_type = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $receipt_type->label(),
      '#description' => $this->t('Label for the receipt type.'),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $receipt_type->id(),
      '#machine_name' => array(
        'exists' => array($this->receiptTypeStorage, 'load'),
        'source' => array('label'),
      ),
      '#disabled' => !$receipt_type->isNew(),
    );

    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $receipt_type->getDescription(),
      '#description' => $this->t('Description of this receipt type'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $receipt_type = $this->entity;

    try {
      $receipt_type->save();
      drupal_set_message($this->t('Saved the %label receipt type.', array(
        '%label' => $receipt_type->label(),
      )));
      $form_state->setRedirect('entity.commerce_receipt_type.collection');
    }
    catch (\Exception $e) {
      $this->logger('commerce_receipt')->error($e);
      drupal_set_message($this->t('The %label receipt type was not saved.', array(
        '%label' => $receipt_type->label(),
      )), 'error');
      $form_state->setRebuild();
    }
  }

}
