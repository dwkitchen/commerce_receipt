<?php

/**
 * @file
 * Contains \Drupal\commerce_receipt\Form\CommerceReceiptTypeDeleteForm.
 */

namespace Drupal\commerce_receipt\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete an receipt type.
 */
class CommerceReceiptTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * The query factory to create entity queries.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Constructs a new CommerceReceiptTypeDeleteForm object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query object.
   */
  public function __construct(QueryFactory $query_factory) {
    $this->queryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $num_receipts = $this->queryFactory->get('commerce_receipt')
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    if ($num_receipts) {
      $caption = '<p>' . $this->formatPlural($num_receipts, '%type is used by 1 receipt on your site. You can not remove this receipt type until you have removed all of the %type receipts.', '%type is used by @count receipts on your site. You may not remove %type until you have removed all of the %type receipts.', array('%type' => $this->entity->label())) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = array('#markup' => $caption);
      return $form;
    }
    return parent::buildForm($form, $form_state);
  }

}
