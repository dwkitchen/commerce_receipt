<?php
/**
 * @file
 * Definition of Drupal\commerce_receipt\Form\CommerceReceiptForm.
 */
namespace Drupal\commerce_receipt\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the commerce_receipt entity edit forms.
 */
class CommerceReceiptForm extends ContentEntityForm {

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, FormStateInterface $form_state) {
    try {
      $this->entity->save();
      drupal_set_message($this->t('The receipt %receipt_label has been successfully saved.', array('%receipt_label' => $this->entity->label())));
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('The receipt %receipt_label could not be saved.', array('%receipt_label' => $this->entity->label())), 'error');
      $this->logger('commerce_receipt')->error($e);
    }
    $form_state->setRedirect('entity.commerce_receipt.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\commerce_receipt\Entity\CommerceReceipt $receipt */
    $receipt = $this->entity;
    $current_user = $this->currentUser();

    $form['advanced'] = array(
      '#type' => 'vertical_tabs',
      '#attributes' => array('class' => array('entity-meta')),
      '#weight' => 99,
    );
    $form = parent::form($form, $form_state);

    $form['receipt_status'] = array(
      '#type' => 'details',
      '#title' => t('Receipt status'),
      '#group' => 'advanced',
      '#attributes' => array(
        'class' => array('receipt-form-receipt-status'),
      ),
      '#attached' => array(
        'library' => array('commerce_receipt/drupal.commerce_receipt'),
      ),
      '#weight' => 90,
      '#optional' => TRUE,
    );

    if (isset($form['status'])) {
      $form['status']['#group'] = 'receipt_status';
    }

    $form['revision'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revision'),
      '#default_value' => $receipt->isNewRevision(),
      '#access' => $current_user->hasPermission('administer products'),
      '#group' => 'receipt_status',
      '#weight' => 10,
    );

    $form['revision_log'] += array(
      '#states' => array(
        'visible' => array(
          ':input[name="revision"]' => array('checked' => TRUE),
        ),
      ),
      '#group' => 'receipt_status',
    );

    // Receipt authoring information for administrators.
    $form['author'] = array(
      '#type' => 'details',
      '#title' => t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => array(
        'class' => array('receipt-form-author'),
      ),
      '#attached' => array(
        'library' => array('commerce_receipt/drupal.commerce_receipt'),
      ),
      '#weight' => 91,
      '#optional' => TRUE,
    );

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['mail'])) {
      $form['mail']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    return $form;
  }

}
