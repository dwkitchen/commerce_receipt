<?php

namespace Drupal\commerce_receipt\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for receipt types.
 */
interface ReceiptTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the receipt type's workflow ID.
   *
   * Used by the $receipt->state field.
   *
   * @return string
   *   The receipt type workflow ID.
   */
  public function getWorkflowId();

  /**
   * Sets the workflow ID of the receipt type.
   *
   * @param string $workflow_id
   *   The workflow ID.
   *
   * @return $this
   */
  public function setWorkflowId($workflow_id);

}
