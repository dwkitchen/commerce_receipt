<?php

namespace Drupal\commerce_receipt\Entity;

use Drupal\commerce_store\Entity\EntityStoreInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Defines the interface for receipts.
 */
interface ReceiptInterface extends EntityStoreInterface, EntityChangedInterface, EntityInterface, EntityOwnerInterface {

  /**
   * Gets the receipt number.
   *
   * @return string
   *   The receipt number.
   */
  public function getReceiptNumber();

  /**
   * Sets the receipt number.
   *
   * @param string $receipt_number
   *   The receipt number.
   *
   * @return $this
   */
  public function setReceiptNumber($receipt_number);

  /**
   * Gets the receipt state.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The receipt state.
   */
  public function getState();

  /**
   * Gets the receipt creation timestamp.
   *
   * @return int
   *   Creation timestamp of the receipt.
   */
  public function getCreatedTime();

  /**
   * Sets the receipt creation timestamp.
   *
   * @param int $timestamp
   *   The receipt creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the line items.
   *
   * @return \Drupal\commerce_receipt\Entity\LineItemInterface[]
   *   The line items.
   */
  public function getLineItems();

  /**
   * Sets the line items.
   *
   * @param \Drupal\commerce_receipt\Entity\LineItemInterface[] $line_items
   *   The line items.
   *
   * @return $this
   */
  public function setLineItems(array $line_items);

  /**
   * Gets whether the receipt has line items.
   *
   * @return bool
   *   TRUE if the receipt has line items, FALSE otherwise.
   */
  public function hasLineItems();

  /**
   * Adds a line item.
   *
   * @param \Drupal\commerce_receipt\Entity\LineItemInterface $line_item
   *   The line item.
   *
   * @return $this
   */
  public function addLineItem(LineItemInterface $line_item);

  /**
   * Removes a line item.
   *
   * @param \Drupal\commerce_receipt\Entity\LineItemInterface $line_item
   *   The line item.
   *
   * @return $this
   */
  public function removeLineItem(LineItemInterface $line_item);

  /**
   * Checks whether the receipt has a given line item.
   *
   * @param \Drupal\commerce_receipt\Entity\LineItemInterface $line_item
   *   The line item.
   *
   * @return bool
   *   TRUE if the line item was found, FALSE otherwise.
   */
  public function hasLineItem(LineItemInterface $line_item);

  /**
   * Gets the additional data stored in this receipt.
   *
   * @return array
   *   An array of additional data.
   */
  public function getData();

  /**
   * Sets random information related to this receipt.
   *
   * @param array $data
   *   An array of additional data.
   *
   * @return $this
   */
  public function setData($data);

  /**
   * Gets the receipt IP address.
   *
   * @return string
   *   The IP address.
   */
  public function getIpAddress();

  /**
   * Sets the receipt IP address.
   *
   * @param string $ip_address
   *   The IP address.
   *
   * @return $this
   */
  public function setIpAddress($ip_address);

  /**
   * Gets the email address associated with the receipt.
   *
   * @return string
   *   The receipt mail.
   */
  public function getEmail();

  /**
   * Sets the receipt mail.
   *
   * @param string $mail
   *   The email address associated with the receipt.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Gets the timestamp of when the receipt was placed.
   *
   * @return int
   *   The timestamp of when the receipt was placed.
   */
  public function getPlacedTime();

  /**
   * Sets the timestamp of when the receipt was placed.
   *
   * @param int $timestamp
   *   The timestamp of when the receipt was placed.
   *
   * @return $this
   */
  public function setPlacedTime($timestamp);

  /**
   * Gets the billing profile.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The billing profile entity.
   */
  public function getBillingProfile();

  /**
   * Sets the billing profile.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The billing profile entity.
   *
   * @return $this
   */
  public function setBillingProfile(ProfileInterface $profile);

  /**
   * Gets the billing profile id.
   *
   * @return int
   *   The billing profile id.
   */
  public function getBillingProfileId();

  /**
   * Sets the billing profile id.
   *
   * @param int $billingProfileId
   *   The billing profile id.
   *
   * @return $this
   */
  public function setBillingProfileId($billingProfileId);

}
