<?php

namespace Drupal\commerce_receipt\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the receipt type entity class.
 *
 * @ConfigEntityType(
 *   id = "commerce_receipt_type",
 *   label = @Translation("Receipt type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\commerce_receipt\Form\ReceiptTypeForm",
 *       "edit" = "Drupal\commerce_receipt\Form\ReceiptTypeForm",
 *       "delete" = "Drupal\commerce_receipt\Form\ReceiptTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\commerce_receipt\ReceiptTypeListBuilder",
 *   },
 *   admin_permission = "administer receipt types",
 *   config_prefix = "commerce_receipt_type",
 *   bundle_of = "commerce_receipt",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "workflow",
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/config/receipt-types/add",
 *     "edit-form" = "/admin/commerce/config/receipt-types/{commerce_receipt_type}/edit",
 *     "delete-form" = "/admin/commerce/config/receipt-types/{commerce_receipt_type}/delete",
 *     "collection" = "/admin/commerce/config/receipt-types"
 *   }
 * )
 */
class ReceiptType extends ConfigEntityBundleBase implements ReceiptTypeInterface {

  /**
   * The receipt type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The receipt type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The receipt type workflow ID.
   *
   * @var string
   */
  protected $workflow;

  /**
   * {@inheritdoc}
   */
  public function getWorkflowId() {
    return $this->workflow;
  }

  /**
   * {@inheritdoc}
   */
  public function setWorkflowId($workflow_id) {
    $this->workflow = $workflow_id;
    return $this;
  }

}
